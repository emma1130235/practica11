(function () {
    /* globals Phaser:false, BasicGame: false */
    //  Create your Phaser game and inject it into the game div.
    //  We did it in a window.onload event, but you can do it anywhere (requireJS load, anonymous function, jQuery dom ready, - whatever floats your boat)
    //  We're using a game size of 480 x 640 here, but you can use whatever you feel makes sense for your game of course.
    //var game = new Phaser.Game(480, 640, Phaser.AUTO, 'game');
 var game = new Phaser.Game(800, 600, Phaser.AUTO, 'game', { preload: preload, create: create, update: update });
    //  Add the States your game has.
    //  You don't have to do this in the html, it could be done in your Game state too, but for simplicity I'll keep it here.
    game.state.add('Game', BasicGame.Game);

    //  Now start the Game state.
    game.state.start('Game');
    
    
    
   
        

function preload() {

    game.load.image('sky', 'asset/sky.png');
    game.load.image('ground', 'asset/platform.png');
    game.load.image('star', 'asset/star.png');
    game.load.spritesheet('dude', 'asset/dude.png', 32, 48);
    game.load.image('firstaid', 'asset/firstaid.png');
    game.load.audio('starman', ['asset/audio/song.mp3', 'assets/audio/starman.ogg']);
    game.load.spritesheet('enemy', 'asset/baddie.png', 32, 32);
}

var player;
var platforms;
var cursors;
var enemy;

var stars;
var score = 0;
var scoreText;

var lives;
var extraLives = 0;
var liveText;

var enemies;
var flag1;
var flag2;
var flag3;
var bool;
var gameOverText;

var music;
var X;
var Y;

function create() {

    //  We're going to be using physics, so enable the Arcade Physics system
    game.physics.startSystem(Phaser.Physics.ARCADE);

    //  A simple background for our game
    game.add.sprite(0, 0, 'sky');

    //  The platforms group contains the ground and the 2 ledges we can jump on
    platforms = game.add.group();

    //  We will enable physics for any object that is created in this group
    platforms.enableBody = true;

    music = game.add.audio('starman');
    music.loop = true;
    music.play();
    game.input.onDown.add(changeVolume, this);

    X = game.world.width;
    Y = game.world.height;

    // Here we create the ground.
    var ground = platforms.create(0, game.world.height - 64, 'ground');

    //  Scale it to fit the width of the game (the original sprite is 400x32 in size)
    ground.scale.setTo(2, 2);

    //  This stops it from falling away when you jump on it
    ground.body.immovable = true;

    //  Now let's create two ledges
    var ledge = platforms.create(400, 400, 'ground');
    ledge.body.immovable = true;

    ledge = platforms.create(-150, 250, 'ground');
    ledge.body.immovable = true;

    // The player and its settings
    player = game.add.sprite(32, Y - 150, 'dude');

    //  We need to enable physics on the player
    game.physics.arcade.enable(player);

    //  Player physics properties. Give the little guy a slight bounce.
    player.body.bounce.y = 0.2;
    player.body.gravity.y = 300;
    player.body.collideWorldBounds = true;

    //  Our two animations, walking left and right.
    player.animations.add('left', [0, 1, 2, 3], 10, true);
    player.animations.add('right', [5, 6, 7, 8], 10, true);

//tarea agregar tema musical, agregar los gatos (3 o 4), colision player gato desaparecer el gato y decrementear las vidas 
    
    enemies = game.add.group();
    enemies.enableBody = true;
    for (var i = 0; i < 3; i++)
    {
        var enemy;
        //enemies.create(i * 70, 0, 'enemy');
        if (i==0) {
            enemy = game.add.sprite(0, 150, 'enemy');
            flag1 = false;
        }else if (i==1) {
            enemy = game.add.sprite(400, 300, 'enemy');
            flag2 = false;
        }else{
            enemy = game.add.sprite(800, Y-150, 'enemy');
            flag3 = true;
        }
        
        enemies.add(enemy);
        game.physics.arcade.enable(enemy);
        enemy.body.gravity.y = 300;
        enemy.body.collideWorldBounds = true;
        enemy.body.bounce.y = 0.7 + Math.random() * 0.2;


    }

    enemies.forEach(function (enemy){
        enemy.animations.add('left', [0, 1], 10, true);
        enemy.animations.add('right', [2, 3], 10, true);
        enemy.animations.play('left', 5, true);
        enemy.animations.play('right', 5, true);
    }, this);
    //  Finally some stars to collect
    stars = game.add.group();

    //  We will enable physics for any star that is created in this group
    stars.enableBody = true;

    //  Here we'll create 12 of them evenly spaced apart
    for (var i = 0; i < 12; i++)
    {
        //  Create a star inside of the 'stars' group
        var star = stars.create(i * 70, 0, 'star');

        //  Let gravity do its thing
        star.body.gravity.y = 300;

        //  This just gives each star a slightly random bounce value
        star.body.bounce.y = 0.7 + Math.random() * 0.2;
    }

    //  The score
    scoreText = game.add.text(16, 16, 'score: 0', { fontSize: '32px', fill: '#000' });

    //lives to collect
    lives = game.add.group();

    //enable physics for lives
    lives.enableBody = true;

    //create 3 lives
    for (var i = 0; i < 3; i++)
    {
        //  Create a live inside of the 'lives' group
        var live = lives.create( i * game.rnd.integerInRange(0, X), game.rnd.integerInRange(0, Y-96), 'firstaid');
        //  Let gravity do its thing
        live.body.collideWorldBounds = true;
    }

    //The live counting
    liveText = game.add.text(200, 16, 'lives: 0', { fontSize: '32px', fill: '#000' });

    //  Our controls.
    cursors = game.input.keyboard.createCursorKeys();
    bool = false;
}

function update() {

    //  Collide the player and the stars with the platforms
    game.physics.arcade.collide(player, platforms);
    game.physics.arcade.collide(stars, platforms);
    game.physics.arcade.collide(lives, platforms);
    game.physics.arcade.collide(enemies, platforms);

    //  Checks to see if the player overlaps with any of the stars, if he does call the collectStar function
    game.physics.arcade.overlap(player, stars, collectStar, null, this);

    // check overlaps
    game.physics.arcade.overlap(player, lives, extraLive, null, this);
    game.physics.arcade.overlap(player, enemies, playerDies, null, this);

    //  Reset the players velocity (movement)
    player.body.velocity.x = 0;

    if (cursors.left.isDown){
        //  Move to the left
        player.body.velocity.x = -150;

        player.animations.play('left');
    }
    else if (cursors.right.isDown){
        //  Move to the right
        player.body.velocity.x = 150;

        player.animations.play('right');
    }
    else{
        //  Stand still
        player.animations.stop();

        player.frame = 4;
    }
    
    //  Allow the player to jump if they are touching the ground.
    if (cursors.up.isDown && player.body.touching.down){
        player.body.velocity.y = -350;
    }


    for (var i = 0, len = enemies.children.length; i < len; i++) {  
        if (i == 0) {
            if (enemies.children[i].body.x == 0) {
                flag1 = false;
            }
            if (enemies.children[i].body.x == 250-32) {
                flag1 = true;
            }
            if ((enemies.children[i].body.x >= 0 || enemies.children[i].body.x <= 250-32) && (flag1 == false) && (enemies.children[i].body.touching.down)) {
                enemies.children[i].body.x += 1;
                enemies.children[i].animations.play('right', 5, true);
            }
            if ((enemies.children[i].body.x >= 0 || enemies.children[i].body.x <= 250-32) && (flag1 == true) && (enemies.children[i].body.touching.down)) {
                enemies.children[i].body.x -= 1;
                enemies.children[i].animations.play('left', 5, true);
            }
        }
        if (i == 1) {
            if (enemies.children[i].body.x == 400) {
                flag2 = false;
            }
            if (enemies.children[i].body.x == X-32) {
                flag2 = true;
            }
            if ((enemies.children[i].body.x >= 400 || enemies.children[i].body.x <= X-32) && (flag2 == false) && (enemies.children[i].body.touching.down)) {
                enemies.children[i].body.x += 1;
                enemies.children[i].animations.play('right', 5, true);
            }
            if ((enemies.children[i].body.x >= 400 || enemies.children[i].body.x <= X-32) && (flag2 == true) && (enemies.children[i].body.touching.down)) {
                enemies.children[i].body.x -= 1;
                enemies.children[i].animations.play('left', 5, true);
            }
        }
        if (i == 2) {
            if (enemies.children[i].body.x == 0) {
                flag3 = false;
            }
            if (enemies.children[i].body.x == X-32) {
                flag3 = true;
            }
            if ((enemies.children[i].body.x >= 0 || enemies.children[i].body.x <= X-32) && (flag3 == false) && (enemies.children[i].body.touching.down)) {
                enemies.children[i].body.x += 1;
                enemies.children[i].animations.play('right', 5, true);
            }
            if ((enemies.children[i].body.x >= 0 || enemies.children[i].body.x <= X-32) && (flag3 == true) && (enemies.children[i].body.touching.down)) {
                enemies.children[i].body.x -= 1;
                enemies.children[i].animations.play('left', 5, true);
            }
        }
    }
    bool = true;
}

function collectStar (player, star) {
    
    // Removes the star from the screen
    star.kill();

    //  Add and update the score
    score += 10;
    scoreText.text = 'Score: ' + score;

}

function extraLive (player, live){
    live.kill();

    extraLives += 1;
    liveText.text = 'Lives: ' + extraLives;
}

function changeVolume(pointer) {
    if (pointer.y < 100){
        music.mute = false;
    }
    else if (pointer.y < 300){
        music.volume += 0.1;
    }
    else{
        music.volume -= 0.1;
    }
}

function playerDies(player, enemies){
    if (extraLives == 0) {
            player.kill();
            gameOverText = game.add.text(game.world.centerX, game.world.centerY, 'Game Over', { fontSize: '60px', fill: '#FF0000' });
    }else{
       extraLives -= 1; 
       player.reset(32, Y - 150);
    }
    liveText.text = 'Lives: ' + extraLives;
}

})();